/*
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

 It does none of this.  In fact, this example has been shit on, and bastardized into 
 a horribly written piece of yarn and ductape.  The goal was to read state of two inputs
 and override any state of and mqtt stat (for perhaps home automation). The idea being for short term
 "here I come" automation of home lights from a vehicle.  It's invasive and not well thought out.

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <stdio.h>
#include <string.h>
#include "OLED.h"
#include "passwd.h"
#define RST_OLED 16
OLED display(4, 5);


// Update these with values suitable for your network.


struct MQTTMsg
{
    char* topic;
    int state;
    const int pin;
    int row;
    int column;
};

#define TopicCount 2
struct MQTTMsg KitchenSwitch = {"/home/Kitchen_light", 0, D7, 3, 6};
struct MQTTMsg DoorSwitch = {"/home/Front_door", 0, D3, 3 , 8};

struct MQTTMsg Topics[TopicCount] = { KitchenSwitch, DoorSwitch};


// you built a struck, and continue to use const.  idiot. FIX
const int switchPin = D7;
const int doorPin = D3;
int switchState = 0;
int doorState = 0;
int topicState = 0;

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  pinMode(RST_OLED, OUTPUT);
  digitalWrite(RST_OLED, LOW);   // turn D2 low to reset OLED
  delay(50);
  digitalWrite(RST_OLED, HIGH);    // while OLED is running, must set D2 in high

  // Initialize display
  display.begin();

  // Test message
  display.print("wermote!");
  delay(3*1000);
  display.clear();

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  // Setup Switch
  // This should iterate and use struct MQTTMsg. FIX
  pinMode(switchPin, INPUT_PULLUP); 
  pinMode(doorPin, INPUT_PULLUP); 
  

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //Serial.println("");
  display.print("WiFi Connected",0);
  // display.print("IP address: ", 1);
  char result[16];
  sprintf(result, "%3d.%3d.%d.%d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
  display.print(result, 1);
}


void callback(char* topic, byte* payload, unsigned int length) {

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

    for ( int i=0; i < TopicCount; i++) {
        
        // handle binary switch values
        if ( strcmp(topic, Topics[i].topic) == 0 ) {
            if ((char)payload[0] == '1' || (char)payload[0] == '0' ) {
                Topics[i].state = atoi((const char *)payload) % 2;
            }
        }
    }
  
  /*
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    //display.print("1", 2);
    Serial.print("Hi there");
    // write something digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is acive low on the ESP-01)
  } else {
    Serial.print("Nope");
    // write something digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }
    */
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("car", "mr2");
      // ... and resubscribe
      for ( int i=0; i < TopicCount; i++) {
        client.subscribe(Topics[i].topic);
      }
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 2 seconds before retrying
      delay(2000);
    }
  }
}
void loop() {
    if (WiFi.status() != WL_CONNECTED) {
        display.print("Wifi Lost Fucker.",0);
        value = 0;
        setup_wifi();
    } else {

        if (!client.connected()) {
            reconnect();
        } else {
            client.loop();


            long now = millis();
            char buf [1];
            int pinState;
            if (now - lastMsg > 600) {
                for ( int i=0; i < TopicCount; i++) {
                    pinState = digitalRead(Topics[i].pin);
                    sprintf (buf, "%d", pinState);
                    if (Topics[i].state  != pinState) {
                        client.publish(Topics[i].topic, buf, 1);
                    }
                    display.print(buf, Topics[i].row, Topics[i].column);
                }
                /*
                if (DoorSwitch.state  != digitalRead(DoorPin)){
                    DoorSwitch.state = !DoorSwitch.state;
                    sprintf (buf, "%d", DoorSwith.state);
                    client.publish(DoorSwitch.topic, buf, 1);
                }
                if (doorState != digitalRead(doorPin)){
                    doorState = !doorState;
                    sprintf (buf, "%d", doorState);
                    client.publish("/home/Front_door", buf, 1);
                }
                */
                lastMsg = now;
                ++value;
                snprintf (msg, 75, "%ld", value % 2);
                Serial.println(msg);
                display.print(msg, 3, 4);
                client.publish("outTopic", msg);
                /*
                sprintf (buf, "%d", switchState);
                display.print(buf, 3, 6);
                sprintf (buf, "%d", doorState);
                display.print(buf, 3, 8);
                */

                //client.publish("/home/Front_door", msg );
            }
        }
    }
}
